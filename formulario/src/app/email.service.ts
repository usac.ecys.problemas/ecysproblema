import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  constructor(private http:HttpClient){
  }

  Email(argument:any){
    return this.http.post('httpspakainfo.com/email/',argument)
    .subscribe(res=>{
      console.log(res);
    },err=>{
      console.log(err)
    })
  }
}

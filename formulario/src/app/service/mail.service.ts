import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MailService {
  private API = 'https://ecys-problemas.herokuapp.com/';
  
  constructor(private http: HttpClient) { }

  send (data:any){
    const headerDict = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Access-Control-Allow-Headers': 'Content-Type',
    }
    const requestOptions = {                                                                                                                                                                                 
      headers: new HttpHeaders(headerDict), 
    };
    console.log(data)
    const data1 = JSON.stringify(data)
    return this.http.post(this.API+'send',data,requestOptions)
  }
}

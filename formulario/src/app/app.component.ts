import { Component } from '@angular/core';
import { EmailService } from './email.service';
import {MailService} from './service/mail.service'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'form';
  alumno = "";
  alumnoc = "";
  area : any = 0;
  curso : any;
  type_pro : any = 0;
  bing = false;
  bproblem = false;
  ing : any;
  problem : any = ""
  correo : any = ""
  ingp : any ;
  body : any = "";
  FomularioY:boolean = false;
  problemDesciption: any ="";
  ingeAttend : any ="";

  ingenieros : any = {
    ingeniero:[
      {
        correo:"",
        nombre:"Seleccione una opcion..."
      },
      {
        correo:"",
        nombre:"Marlon Francisco Orellana"
      },
      {
        correo:"",
        nombre:"Byron Rodolfo Zepeda"
      },
      {
        correo:"",
        nombre:"Moises Eduardo Velasquez"
      },
      {
        correo:"",
        nombre:"Herman Igor Veliz"
      },
      {
        correo:"",
        nombre:"Neftali de Jesús Calderon"
      },
      {
        correo:"",
        nombre:"William Estuardo Escobar"
      },
      {
        correo:"",
        nombre:"Virginia Victoria Tala"
      },
      {
        correo:"",
        nombre:"Floriza Felipa Avila"
      },
      {
        correo:"",
        nombre:"Vivian Damaris Campos"
      },
      {
        correo:"",
        nombre:"Otto Amilcar Rodriguez"
      },
      {
        correo:"",
        nombre:"Zulma Karina Aguirre"
      },
      {
        correo:"",
        nombre:"David Estuardo Morales"
      },
      {
        correo:"",
        nombre:"Marlon Antonio Perez"
      },
      {
        correo:"",
        nombre:"Calaudia Liceth Rojas"
      },
      {
        correo:"",
        nombre:"José Manuel Ruiz"
      },
      {
        correo:"",
        nombre:"Edwin Estuardo Zapeta"
      },
      {
        correo:"",
        nombre:"Manuel Haroldo Castillo"
      },
      {
        correo:"",
        nombre:"Kevin Adiel Lajpop"
      },
      {
        correo:"",
        nombre:"Otto Rene Escobar"
      },
      {
        correo:"",
        nombre:"Jesus Alberto Guzman"
      },
      {
        correo:"",
        nombre:"Alvaro Obrayan Hernandez"
      },
      {
        correo:"",
        nombre:"Luis Fernando Espino"
      },
      {
        correo:"",
        nombre:"Jorge Luis Alvarez"
      },
      {
        correo:"",
        nombre:"Bayron Wosvely Lopez"
      },
      {
        correo:"",
        nombre:"Edgar Ruben Saban"
      },
      {
        correo:"",
        nombre:"Erick Carlos Navarro"
      },
      {
        correo:"",
        nombre:"Juan Alvaro Diaz"
      },
      {
        correo:"",
        nombre:"Oscar Alejandro Paz"
      },
      {
        correo:"",
        nombre:"Sergio Arnaldo Mendez"
      },
      {
        correo:"",
        nombre:"Gabriel Alejandro Diaz"
      },
      {
        correo:"",
        nombre:"Allan Alberto Morataya"
      },
      {
        correo:"",
        nombre:"Alvaro Giovanni Longo"
      },
      {
        correo:"",
        nombre:"Cesar Rolando Batz"
      },
      {
        correo:"",
        nombre:"Pedro Pablo Hernandez"
      },
      {
        correo:"",
        nombre:"Luis Alberto Arias"
      },
      {
        correo:"",
        nombre:"Mirna Ivonne Aldana"
      },
      {
        correo:"",
        nombre:"William Samuel Guevara"
      },
      {
        correo:"",
        nombre:"Manuel Fernando Lopez"
      },
      {
        correo:"",
        nombre:"Cesar Augusto Fernandez"
      },
      {
        correo:"",
        nombre:"Luis Alberto Vettorazzi"
      },
      {
        correo:"",
        nombre:"Miguel Angel Cancinos"
      },
      {
        correo:"",
        nombre:"Everest Darwin Medinilla"
      }
    ]
  }
  problemas : any = {
    problemaa :[
      {
        problema: "No imparte correctamente el Lab."
      },
      {
        problema: "No se atienden mis dudas"
      },
      {
        problema: "Prepotencia o conductas negativas por parte del auxiliar"
      },
      {
        problema: "Se reusaron a calificarme en el horario que se especificó"
      },
      {
        problema: "Inconformidad con una nota"
      },
      {
        problema: "No recibo soporte del auxiliar"
      },
      {
        problema: "No se presenta a dar clases"
      }
    ],
    problemai : [
      {
        problema: "No se presenta a dar clases"
      },
      {
        problema: "No se cubren los temas del curso"
      },
      {
        problema: "No se evalua correctamente el contenido"
      },
      {
        problema: "No se imparten las notas del curso"
      },
      {
        problema: "No existe evidencia con las notas del curso"
      },
      {
        problema: "Las notas del sistema no cuadran con las impartidas"
      }
    ] 
  }
  constructor(private emailService : EmailService, private mail:MailService){

  }

  cambioEstado(event : any) {
    this.type_pro = event.target.value;
  }
  cambioEstadoP(){
    this.bproblem = true;
  }
  cambioEstadoI(){
    this.bing = true;
  }
  sendMail(){
    
    if(this.type_pro == 2){
      console.log("problemas de inge")
      console.log(this.area)
      if(this.area == 1){
          console.log(this.area)
          this.correo = "juanluis.juanluis.robles1@gmail.com"
          this.ingeAttend = "Marlon Orellana"
         
        }
        if(this.area == 2){
          console.log(this.area)
          this.correo = "juanluis.juanluis.robles1@gmail.com"
          this.ingeAttend = "Cesar Fernandez"

        }
        if(this.area == 3){
          console.log(this.area)
          this.correo = "juanluis.juanluis.robles1@gmail.com"
          this.ingeAttend = "Luis Espino"
         }
      this.body = `
        <html>
          <body>
          <h1>Problemas con ingeniero de curso</h1>
            <table style = "boder:1px solid black;">
              <tr>
                <td> </td>
                <td> </td>
              </tr>
              <tr>
                <td>Curso</td>
                <td>${this.curso}</td>
              </tr>
              <tr>
                <td>Ingeniero</td>
                <td>${this.ingp}</td>
              </tr>
              <tr>
                <td>Comunicarse con el alumno a</td>
                <td>${this.alumnoc}</td>
              </tr>
              <tr>
                <td>Problema</td>
                <td>${this.problem}</td>
              </tr>
              <tr>
                <td>Detalle</td>
                <td>${this.problemDesciption}</td>
              </tr>
            </table>
          <body>
        </html>
      `
        let emailSend = {
          from : "usac.ecys.problemas@gmail.com",
          send_to: this.correo,
          subject : "Atención al estudiante, problema con ingenieros",
          body: this.body
        }
        //mail sended to Area coordinator
        this.mail.send(emailSend).subscribe((res)=>{
          this.FomularioY = true;
        }
        ,
        (err)=>{
          console.log(err);
        })
        let body2  = `
        <html>
        <head></head>
          <body>
          <h1>Problemas con ingeniero de curso</h1>
            <table>
              <tr>
                <td> </td>
                <td> </td>
              </tr>
              <tr>
                <td>Buen día su problema será atendido por el ingeniero ${this.ingeAttend}</td>
              </tr>
              <tr>
                <td>Se comunicará con usted por el siguiente correo</td>
                <td>${this.correo}</td>
              </tr>
            </table>
          <body>
        </html>
      `
        let emailSend2 = {
          from : "usac.ecys.problemas@gmail.com",
          send_to: this.alumnoc,
          subject : "Atención al estudiante, problema con ingenieros",
          body: body2
        }
        //mail sended to Area coordinator
        this.mail.send(emailSend2).subscribe((res)=>{
          this.FomularioY = true;
        }
        ,
        (err)=>{
          console.log(err);
        })
    }
    else{
      this.body = `
        <html>
          <body>
          <h1>Problemas con auxiliar de curso</h1>
            <table style = "boder:1px solid black;">
              <tr>
                <td> </td>
                <td> </td>
              </tr>
              <tr>
                <td>Curso</td>
                <td>${this.curso}</td>
              </tr>
              <tr>
                <td>Comunicarse con el alumno a</td>
                <td>${this.alumnoc}</td>
              </tr>
              <tr>
                <td>Problema</td>
                <td>${this.problem}</td>
              </tr>
              <tr>
                <td>Detalle</td>
                <td>${this.problemDesciption}</td>
              </tr>
            </table>
          <body>
        </html>
      `

      let body2 = `
        <html>
        <head></head>
          <body>
          <h1>Problemas con auxiliar de curso</h1>
            <table>
              <tr>
                <td> </td>
                <td> </td>
              </tr>
              <tr>
                <td>Buen día su problema será atendido por el ingeniero Miguel Marín</td>
              </tr>
              <tr>
                <td>Se comunicará con usted por el siguiente correo</td>
                <td>mmarin2@gmail.com</td>
              </tr>
            </table>
          <body>
        </html>
      `
      this.correo = "juanluis.juanluis.robles1@gmail.com"
        let emailSend = {
          from : "usac.ecys.problemas@gmail.com",
          send_to: this.correo,
          subject : "Atención al estudiante, Problema Auxiliar",
          body: this.body
        }
        //mail sended to ing Miguel Marin
        this.mail.send(emailSend).subscribe((res)=>{
          
        },(error)=>{
          console.log(error)
        })

        let emailSend2 = {
          from : "usac.ecys.problemas@gmail.com",
          send_to: this.alumnoc,
          subject : "Atención al estudiante, Problema Auxiliar",
          body: body2
        }
        //mail sended to student
        this.mail.send(emailSend2).subscribe((res)=>{
          this.FomularioY = true;
        },(error)=>{
          console.log(error)
        })
    }
  }
}

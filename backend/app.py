from flask import Flask, request
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email import encoders
from flask_cors import CORS
import traceback

app = Flask(__name__)
CORS(app)
####################################################################################
# "IMPORTANT: WHEN SENDING EMAILS FROM GMAI ENSURE THAT YOU HAVE ENABLED LESS SECURE APPS "
send_from =  "usac.ecys.problemas@gmail.com"
send_to = "luisrobles2408@gmail.com"
subject = "Atención al estudiante"
body = "This is body of email"
username  =  "usac.ecys.problemas@gmail.com"
password =  "usacecys2021"     #heroku pwd: usacecys2021+ #gh token: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
# https://gitlab.com/usac.ecys.problemas/ecysproblema.git


@app.route("/test")
def hello():
    return "Hello, World!"

@app.route("/send",methods = ['POST'])
def test():
    try:
        data = request.get_json()
        print((data))
        msg = MIMEMultipart()

        msg['From'] = send_from
        msg['To'] = data["send_to"]
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = subject
        server = "smtp.gmail.com"
        port = 587
        use_tls = True

        #msg.attach(MIMEText(data["body"]))
        msg.attach(MIMEText(data["body"] ,'html'))

        #### ATTACHMENT CODE ***** COMMENT OUT CODE BELOW TO EXCLUDE ATTACHMENT *******
        '''
        part = MIMEBase('application', "octet-stream")
        with open(attachmentPath, 'rb') as file:
            part.set_payload(file.read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition',
                        'attachment; filename="{}"'.format(Path(attachmentPath).name))
        msg.attach(part)
        '''
        #### COMMENT OUT CODE ABOVE TO EXCLUDE ATTACHMENT *******

        smtp = smtplib.SMTP(server, port)
        if use_tls:
            smtp.starttls()
            smtp.login(username, password)
            smtp.sendmail(send_from, msg["To"], msg.as_string())
            smtp.quit()

            print (msg["To"])
            return {"Message":"Success"}
    except:
        error = traceback.format_exc()
        return {"Message":error}